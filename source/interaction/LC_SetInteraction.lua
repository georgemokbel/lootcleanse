-- LC_SetInteraction.lua
-- Contents.
-- 0. Interactions

----------------------------------------------------------------------------
-- Interactions ------------------------------------------------------------
----------------------------------------------------------------------------

local settings = lc.frames.settings

settings:EnableMouse(true)
settings:SetClampedToScreen(true)
settings:SetMovable(true)
settings:SetResizable(false)
settings:SetToplevel(true)
settings:SetUserPlaced(true)

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------