-- LC_SlashCommands

local settings = lc.frames.settings
-- Create slash command to open GUI.
SLASH_LC_LC1 = "/lc"

SlashCmdList["LC_LC"] = function() 
  if settings:IsVisible() then
    settings:SetShown(false)
  else
    settings:SetShown(true)
  end
end


