-- LC_RegisterWidgets.lua
-- Contents
-- 0. Settings Registration
-- 1. Logic Registration

local settings = lc.frames.settings
local options = lc.frames.settings.options 
local content = lc.frames.settings.content
local logic = lc.frames.logic

----------------------------------------------------------------------------
-- Settings Registration ---------------------------------------------------
----------------------------------------------------------------------------

settings:RegisterEvent("ADDON_LOADED")
settings:RegisterEvent("PLAYER_LOGIN")
settings:RegisterForDrag("LeftButton")

settings:RegisterForDrag("LeftButton")

options.QualityButton:RegisterForClicks("LeftButtonUp")
options.VendorButton:RegisterForClicks("LeftButtonUp")
options.ExemptionsButton:RegisterForClicks("LeftButtonUp")
options.MiscButton:RegisterForClicks("LeftButtonUp")
options.HelpButton:RegisterForClicks("LeftButtonUp")

-- content.EnableCloseWithEscCB:RegisterForClicks("LeftButtonUp")

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

----------------------------------------------------------------------------
-- Logic Registration -------------------------------------------------------
----------------------------------------------------------------------------

logic:RegisterEvent("LOOT_OPENED")
logic:RegisterEvent("CHAT_MSG_LOOT")
logic:RegisterEvent("MERCHANT_SHOW")

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------