-- LC_InitializeResources.lua
-- Contents
-- 0. Load Textures
-- 1. Load Backdrops
-- 2. Load Strings

local locale = lc.localization
local settings = lc.frames.settings
local options = lc.frames.settings.options
local content = lc.frames.settings.content
local help = lc.frames.settings.content.helpmessage

----------------------------------------------------------------------------
-- Load Textures -----------------------------------------------------------
----------------------------------------------------------------------------

-- TitleBox
settings.TITLE_BOX:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header")
-- Horizontal Lines
options.HL1_LEFT = HorizontalLineLeft( options )
options.HL1_RIGHT = HorizontalLineRight( options )
content.HL1_LEFT = HorizontalLineLeft( content )
content.HL1_RIGHT = HorizontalLineRight( content )
content.HL2_LEFT = HorizontalLineLeft( content )
content.HL2_RIGHT = HorizontalLineRight( content )
content.HELP_HL1_LEFT = HorizontalLineLeft( content )
content.HELP_HL1_RIGHT = HorizontalLineRight( content )
-- Icons
content.GOLD_ICON:SetTexture("Interface\\MONEYFRAME\\UI-GoldIcon")
content.SILVER_ICON:SetTexture("Interface\\MONEYFRAME\\UI-SilverIcon")
content.COPPER_ICON:SetTexture("Interface\\MONEYFRAME\\UI-CopperIcon")

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

----------------------------------------------------------------------------
-- Load Backdrops -----------------------------------------------------------
----------------------------------------------------------------------------


settings.backdrop = {
    bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
    edgeFile = "Interface\\DialogFrame\\UI-DialogBox-Border",
    tile = true,
    tileSize = 16,
    edgeSize = 24,
  }
  
options.backdrop = {
    bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
    edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border",
    tile = true,
    tileSize = 8,
    edgeSize = 8,
  }
  
content.backdrop = {
    bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
    edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border",
    tile = true,
    tileSize = 8,
    edgeSize = 8,
  }

  
----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

----------------------------------------------------------------------------
-- Load Strings ------------------------------------------------------------
----------------------------------------------------------------------------

settings.TITLE_BOX_STRING:SetFontObject(GameFontNormal)
settings.TITLE_BOX_STRING:SetText("LootCleanse")
settings.FOOTER_STRING:SetFontObject(GameFontRed)
settings.FOOTER_STRING:SetText(locale["SettingsNote"])
settings.FOOTER_STRING:SetJustifyH("LEFT")

settings.SaveButton:SetText(locale["Save"])
settings.CloseButton:SetText(locale["Close"])


options.TITLE_STRING:SetFontObject(ErrorFont)
options.TITLE_STRING:SetText(locale["Settings"])

content.TITLE_STRING:SetFontObject(GameFontNormalLarge)
content.TITLE_STRING:SetText(locale["Quality"])

content.INSTRUCTIONS_STRING:SetFontObject(GameFontWhite)
content.INSTRUCTIONS_STRING:SetText(locale["QualityInstructions"])
content.MISC_FOOTER:SetFontObject(GameFontRedSmall)
content.MISC_FOOTER:SetText(locale["MiscellaneousFooter"])
content.MISC_FOOTER:SetWordWrap(true)
content.MISC_FOOTER:SetWidth( 272 )

content.DELETE_STRING:SetFontObject(GameFontNormal)
content.DELETE_STRING:SetText(locale["Delete"])
content.DND_STRING:SetFontObject(GameFontNormal)
content.DND_STRING:SetText(locale["Do Not Delete"])

help.MESSAGE1:SetFontObject(GameFontWhite)
help.MESSAGE1:SetText(locale["Help-Default"])
help.MESSAGE1:SetWidth( 320 )
help.MESSAGE2:SetFontObject(GameFontWhite)
help.MESSAGE2:SetText(locale["empty"])
help.MESSAGE2:SetWidth( 320 )
help.MESSAGE3:SetFontObject(GameFontWhite)
help.MESSAGE3:SetText(locale["empty"])
help.MESSAGE3:SetWidth( 320 )


----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------