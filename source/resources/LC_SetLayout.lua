-- LC_SetLayout.lua
-- Contents
-- 0. Settings Layout
-- 1. Help Layout

local locale = lc.localization
local settings = lc.frames.settings
local options = lc.frames.settings.options
local content = lc.frames.settings.content
local help = lc.frames.settings.content.helpmessage

----------------------------------------------------------------------------
-- Settings ----------------------------------------------------------------
----------------------------------------------------------------------------

settings:SetSize( 512, 384 )
settings:SetPoint("CENTER")
settings:SetBackdrop(settings.backdrop)

settings.TITLE_BOX:SetPoint( "CENTER", settings, "TOP", 0, -16 )
settings.TITLE_BOX_STRING:SetPoint( "CENTER", settings.TITLE_BOX, "CENTER", 0, 12 )
settings.FOOTER_STRING:SetPoint( "TOPLEFT", settings.CloseButton, "BOTTOMLEFT", 0, -8)
settings.FOOTER_STRING:SetWordWrap(true)
settings.FOOTER_STRING:SetWidth(128)

settings:Hide()

settings.SaveButton:SetWidth(128)
settings.SaveButton:SetPoint("TOPLEFT", options, "BOTTOMLEFT", 0, -8)
settings.CloseButton:SetWidth(128)
settings.CloseButton:SetPoint("TOPLEFT", settings.SaveButton, "BOTTOMLEFT", 0, -4)


---------------------------------------------------------------------------
options:SetSize( 128, 172 )
options:SetPoint("TOPLEFT", settings, "TOPLEFT", 16, -32)
options:SetBackdrop(options.backdrop)
options:SetBackdropBorderColor( 167, 167, 167, .5 )

options.TITLE_STRING:SetPoint("TOP", options, "TOP", 0, -8)
options.HL1_LEFT:SetPoint("TOPRIGHT", options.TITLE_STRING, "BOTTOM", 0, -4)
options.HL1_RIGHT:SetPoint("LEFT", options.HL1_LEFT, "RIGHT", 0, 0)

options.QualityButton = ListButtonTemplate( options:GetName()..": Quality", options, "Quality" )
options.QualityButton:SetPoint("TOP", options.TITLE_STRING, "BOTTOM", 0, -6)
options.QualityButton:SetNormalFontObject(GameFontWhite)

options.VendorButton = ListButtonTemplate( options:GetName()..": Vendor", options, "Vendor" )
options.VendorButton:SetPoint("TOPLEFT", options.QualityButton, "BOTTOMLEFT", 0, -4)

options.ExemptionsButton = ListButtonTemplate( options:GetName()..": Exemptions", options, "Exemptions" )
options.ExemptionsButton:SetPoint("TOPLEFT", options.VendorButton, "BOTTOMLEFT", 0, -4)

options.MiscButton = ListButtonTemplate( options:GetName()..": Miscellaneous", options, "Miscellaneous" )
options.MiscButton:SetPoint("TOPLEFT", options.ExemptionsButton, "BOTTOMLEFT", 0, -4)

options.HelpButton = ListButtonTemplate( options:GetName()..": Help", options, "Help" )
options.HelpButton:SetPoint("TOPLEFT", options.MiscButton, "BOTTOMLEFT", 0, -4)
---------------------------------------------------------------------------

content:SetSize( 336, 336 )
content:SetPoint("TOPLEFT", options, "TOPRIGHT", 8, 0)
content:SetBackdrop(content.backdrop)
content:SetBackdropBorderColor(255,255,255,.75)

content.HL1_LEFT:SetPoint("TOPRIGHT", content, "TOP", 0, -24)
content.HL1_RIGHT:SetPoint("LEFT", content.HL1_LEFT, "RIGHT", 0, 0)
content.HL2_LEFT:SetPoint("BOTTOMRIGHT", content, "BOTTOM", 0, 8)
content.HL2_RIGHT:SetPoint("LEFT", content.HL2_LEFT, "RIGHT", 0, 0)
content.HELP_HL1_LEFT:SetPoint("TOPRIGHT", content.TITLE_STRING, "CENTER", 0, -48 )
content.HELP_HL1_RIGHT:SetPoint("LEFT", content.HELP_HL1_LEFT, "RIGHT", 0, 0)

content.TITLE_STRING:SetPoint("TOP", content, "TOP", 0, -6)

content.INSTRUCTIONS_STRING:SetPoint("TOP", content.TITLE_STRING, "BOTTOM", 0, -4)
content.INSTRUCTIONS_STRING:SetWordWrap(true)
content.INSTRUCTIONS_STRING:SetWidth(324)

content.KeepPoorCB = CheckBoxButtonTemplate("Poor", content)
content.KeepCommonCB = CheckBoxButtonTemplate("Common", content)
content.KeepUncommonCB = CheckBoxButtonTemplate("Uncommon", content)
content.KeepRareCB = CheckBoxButtonTemplate("Rare", content)
content.KeepEpicCB = CheckBoxButtonTemplate("Epic", content)
content.KeepQuestCB = CheckBoxButtonTemplate("Quest", content)

content.KeepPoorCB:SetPoint("TOPLEFT", content.INSTRUCTIONS_STRING, "BOTTOMLEFT", 16, -8)
content.KeepCommonCB:SetPoint("TOPLEFT", content.KeepPoorCB, "BOTTOMLEFT", 0, -4)
content.KeepUncommonCB:SetPoint("TOPLEFT", content.KeepCommonCB, "BOTTOMLEFT", 0, -4)
content.KeepRareCB:SetPoint("TOPLEFT", content.KeepUncommonCB, "BOTTOMLEFT", 0, -4)
content.KeepEpicCB:SetPoint("TOPLEFT", content.KeepRareCB, "BOTTOMLEFT", 0, -4)
content.KeepQuestCB:SetPoint("TOPLEFT", content.KeepEpicCB, "BOTTOMLEFT", 0, -4)

content.GoldEB = EditBoxTemplate( "Gold", content )
content.GoldEB:SetWidth(32)
content.GoldEB:SetMaxLetters(3)
content.SilverEB = EditBoxTemplate( "Silver", content )
content.CopperEB = EditBoxTemplate( "Copper", content )
content.AutoVendorCB = CheckBoxButtonTemplate("AutoVendor", content)

content.GoldEB:SetPoint("TOPLEFT", content.INSTRUCTIONS_STRING, "BOTTOMLEFT", 88, -8)
content.GOLD_ICON:SetPoint("LEFT", content.GoldEB, "RIGHT", 4, 0)
content.SilverEB:SetPoint("LEFT", content.GOLD_ICON, "RIGHT", 8, 0)
content.SILVER_ICON:SetPoint("LEFT", content.SilverEB, "RIGHT", 4, 0)
content.CopperEB:SetPoint("LEFT", content.SILVER_ICON, "RIGHT", 8, 0)
content.COPPER_ICON:SetPoint("LEFT", content.CopperEB, "RIGHT", 4, 0)
content.AutoVendorCB:SetPoint("TOPLEFT", content.INSTRUCTIONS_STRING, "BOTTOMLEFT", 0, -48 )


content.DoNotDeleteEB = ScrollFrameEditBox( "Do Not Delete", content.DoNotDeleteSF )
content.DeleteEB = ScrollFrameEditBox( "Delete", content.DeleteSF )

content.DoNotDeleteSF:SetScrollChild(content.DoNotDeleteEB)
content.DeleteSF:SetScrollChild(content.DeleteEB)

content.DND_STRING:SetPoint("TOPLEFT", content.INSTRUCTIONS_STRING, "BOTTOMLEFT", 4, -4)

content.DoNotDeleteSF:SetSize( 140, 240 )
content.DoNotDeleteSF:SetPoint("TOPLEFT", content.DND_STRING, "BOTTOMLEFT", -4, -4 )
content.DoNotDeleteSF:SetPoint("BOTTOMRIGHT", content.DELETE_STRING, "LEFT", -24, -240)

content.DELETE_STRING:SetPoint("TOPLEFT", content.INSTRUCTIONS_STRING, "BOTTOM", 4, -4 )

content.DeleteSF:SetPoint("TOPLEFT", content.DELETE_STRING, "BOTTOMLEFT", -4, -4)
content.DeleteSF:SetPoint("BOTTOMRIGHT", content.DoNotDeleteSF, "BOTTOMRIGHT", 160, 0)

content.DoNotDeleteEB:SetPoint("LEFT", content.DoNotDeleteSF)
content.DoNotDeleteEB:SetPoint("RIGHT", content.DoNotDeleteSF)
content.DoNotDeleteEB:SetPoint("TOP", content.DoNotDeleteSF)

content.DeleteEB:SetPoint("LEFT", content.DeleteSF)
content.DeleteEB:SetPoint("RIGHT", content.DeleteSF)
content.DeleteEB:SetPoint("TOP", content.DeleteSF)

content.AutoLootCorpseMoneyCB = CheckBoxButtonTemplate("AutoLootCorpseMoney", content)
content.DisableLootCleanseCB = CheckBoxButtonTemplate("DisableLootCleanse", content)
--content.EnableCloseWithEscCB = CheckBoxButtonTemplate("EnableCloseWithEsc", content)
content.EnableSaveOnCloseCB = CheckBoxButtonTemplate("EnableSaveOnClose", content)

content.AutoLootCorpseMoneyCB:SetPoint("TOPLEFT", content.INSTRUCTIONS_STRING, "BOTTOMLEFT", 16, -8)
content.DisableLootCleanseCB:SetPoint("TOPLEFT", content.AutoLootCorpseMoneyCB, "BOTTOMLEFT", 0, -4)
--content.EnableCloseWithEscCB:SetPoint("TOPLEFT", content.DisableLootCleanseCB, "BOTTOMLEFT", 0, -4)
content.EnableSaveOnCloseCB:SetPoint("TOPLEFT", content.DisableLootCleanseCB, "BOTTOMLEFT", 0, -4)
content.MISC_FOOTER:SetPoint("TOPLEFT", content.EnableSaveOnCloseCB, "BOTTOMLEFT", 0, -8 )

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

----------------------------------------------------------------------------
-- Help --------------------------------------------------------------------
----------------------------------------------------------------------------

help:SetSize( 328, 272 )
help:SetPoint( "CENTER", content, "CENTER", 0, -24)

content.HelpQualityButton = ListButtonTemplate( content:GetName()..": HelpQuality", content, "Quality", 64)
content.HelpQualityButton:SetPoint("TOP", content.TITLE_STRING, "BOTTOM", 0, -4)
content.HelpQualityButton:SetPoint("LEFT", content, "LEFT", 16, 0)
content.HelpExemptionsButton = ListButtonTemplate( content:GetName()..": HelpExemptions", content, "Exemptions", 64)
content.HelpExemptionsButton:SetPoint("LEFT", content.HelpQualityButton, "RIGHT", 4, 0 )
content.HelpVendorButton = ListButtonTemplate( content:GetName()..": HelpVendor", content, "Vendor", 64)
content.HelpVendorButton:SetPoint("LEFT", content.HelpExemptionsButton, "RIGHT", 4, 0)
content.HelpMiscButton = ListButtonTemplate( content:GetName()..": HelpMisc", content, "Miscellaneous", 64)
content.HelpMiscButton:SetPoint("LEFT", content.HelpVendorButton, "RIGHT", 4, 0)

help.MESSAGE1:SetPoint("TOPLEFT", help, "TOPLEFT", 4, -8)
help.MESSAGE2:SetPoint("TOPLEFT", help.MESSAGE1, "BOTTOMLEFT", 0, -8 )
help.MESSAGE3:SetPoint("TOPLEFT", help.MESSAGE2, "BOTTOMLEFT", 0, -8 )


----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------