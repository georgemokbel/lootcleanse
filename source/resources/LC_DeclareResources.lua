-- LC_DeclareResources.lua
-- Note: Anything that is declared nil is created from a template. Those objects are created in LC_SetLayout.lua
-- Contents
-- 1. Create Frames ( typedefs come after since frames are non-existant before creation )
-- 2. Create Textures
-- 3. Create Backdrops
-- 4. Create Strings
-- 5. Create Widgets
-- 6. Templates


----------------------------------------------------------------------------
-- Create Frames -----------------------------------------------------------
----------------------------------------------------------------------------

lc.frames = { }
lc.utility = { n = 0 }

lc.frames.settings = CreateFrame("Frame", "Settings", UIParent)
lc.frames.settings.options = CreateFrame("Frame", lc.frames.settings:GetName()..": Options", lc.frames.settings)
lc.frames.settings.content = CreateFrame("Frame", lc.frames.settings:GetName()..": Content", lc.frames.settings)
lc.frames.settings.content.helpmessage = CreateFrame("Frame", lc.frames.settings.content:GetName()..": Help", lc.frames.settings.content)
lc.frames.logic = CreateFrame("Frame", "Logic", lc.frames.settings)


local settings = lc.frames.settings
local options = lc.frames.settings.options
local content = lc.frames.settings.content
local help = lc.frames.settings.content.helpmessage
local logic = lc.frames.logic

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------



----------------------------------------------------------------------------
-- Create Textures ---------------------------------------------------------
----------------------------------------------------------------------------

-- TitleBox
settings.TITLE_BOX = settings:CreateTexture()
-- Horizontal Lines
options.HL1_LEFT = nil
options.HL1_RIGHT = nil
content.HL1_LEFT = nil
content.HL1_RIGHT = nil
content.HL2_LEFT = nil
content.HL2_RIGHT = nil
content.HELP_HL1_LEFT = nil
content.HELP_HL1_RIGHT = nil
-- Icons
content.GOLD_ICON = content:CreateTexture()
content.SILVER_ICON = content:CreateTexture()
content.COPPER_ICON = content:CreateTexture()

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------



----------------------------------------------------------------------------
-- Create Backdrops --------------------------------------------------------
----------------------------------------------------------------------------

settings.backdrop = { }
  
options.backdrop = { }

content.backdrop = { }

help.backdrop = { }

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------



----------------------------------------------------------------------------
-- Create Strings ----------------------------------------------------------
----------------------------------------------------------------------------

settings.TITLE_BOX_STRING = settings:CreateFontString()

settings.FOOTER_STRING = settings:CreateFontString()

options.TITLE_STRING = options:CreateFontString()

content.TITLE_STRING = content:CreateFontString()
content.INSTRUCTIONS_STRING = content:CreateFontString()
content.MISC_FOOTER = content:CreateFontString()
content.DELETE_STRING = content:CreateFontString()
content.DND_STRING = content:CreateFontString()

help.MESSAGE1 = help:CreateFontString()
help.MESSAGE2 = help:CreateFontString()
help.MESSAGE3 = help:CreateFontString()

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------



----------------------------------------------------------------------------
-- Create Widgets ----------------------------------------------------------
----------------------------------------------------------------------------

options.QualityButton = nil
options.VendorButton = nil
options.ExemptionsButton = nil
options.MiscButton = nil
options.HelpButton = nil

content.KeepPoorCB = nil
content.KeepCommonCB = nil
content.KeepUncommonCB = nil
content.KeepRareCB = nil
content.KeepEpicCB = nil
content.KeepQuestCB = nil

content.GoldEB = nil
content.SilverEB = nil
content.CopperEB = nil
content.AutoVendorCB = nil

content.DoNotDeleteSF = CreateFrame("ScrollFrame", content:GetName()..": DoNotDelete Scroll Frame", content, "UIPanelScrollFrameTemplate")
content.DeleteSF = CreateFrame("ScrollFrame", content:GetName()..": Delete Scroll Frame", content, "UIPanelScrollFrameTemplate")
content.DoNotDeleteEB = nil
content.DeleteEB = nil

content.AutoLootCorpseMoneyCB = nil
content.DisableLootCleanseCB = nil
-- content.EnableCloseWithEscCB = nil
content.EnableSaveOnCloseCB = nil

settings.CloseButton = CreateFrame("Button", settings:GetName()..": Close", settings, "GameMenuButtonTemplate")
settings.SaveButton = CreateFrame("Button", settings:GetName()..": Save", settings, "GameMenuButtonTemplate")

content.HelpQualityButton = nil
content.HelpExemptionsButton = nil
content.HelpVendorButton = nil
content.HelpMiscButton = nil


----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
