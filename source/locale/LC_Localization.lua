-- LC_Localization.lua
-- Contents
-- 0. Acquisition
-- 1. Application

----------------------------------------------------------------------------
-- Acquisition -------------------------------------------------------------
----------------------------------------------------------------------------

-- Create a localization table in addon namespace.
lc.localization = { }

-- Typedefs
-- lt = localization table
local lt = lc.localization

-- Get locale
local locale = GetLocale()

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

----------------------------------------------------------------------------
-- Application -------------------------------------------------------------
----------------------------------------------------------------------------

-- German ( Germany )
if locale == "deDE" then
  
-- French ( France )
elseif locale == "frFR" then
  
-- Korean ( Korea )
elseif locale == "koKR" then
  
-- Chinese (China) (simplified)
elseif locale == "zhCN" then
    
-- Chinese (Taiwan) (traditional)
elseif locale == "zhTW" then
  
-- Russian (Russia)
elseif locale == "ruRU" then
  
-- Spanish (Spain)
elseif locale == "esES" then
  
-- Spanish (Mexico)
elseif locale == "esMX" then
  
-- Portuguese (Brazil)
elseif locale == "ptBR" then
  
else
    -- Settings title
    lt["Settings"] = "Settings"
    -- Settings save tip.
    lt["SettingsNote"] = "Note: Pressing close does not save settings, you must press the save button."
    -- Save button
    lt["Save"] = "Save"
    -- Close button
    lt["Close"] = "Close"
    
    -- Message displayed when player logs in.
    lt["PlayerLogin"] = "Type /lc to load gui."
    -- Quality button
    lt["Quality"] = "Quality"
    -- Vendor button
    lt["Vendor"] = "Vendor"
    -- Exemptions button
    lt["Exemptions"] = "Exemptions"
     -- Help button
    lt["Help"] = "Help"
    -- Miscellaneous
    lt["Miscellaneous"] = "Miscellaneous"
    
    -- Item quality checkboxes
    lt["Poor"] = "Poor"
    lt["Common"] = "Common"
    lt["Uncommon"] = "Uncommon"
    lt["Rare"] = "Rare"
    lt["Epic"] = "Epic"
    lt["Quest"] = "Quest"
    
    -- AutoVendor checkbox
    lt["AutoVendor"] = "Automatically vendor items at merchant."
    
    -- Quality screen instructions
    lt["QualityInstructions"] = "Check the box to delete any items you loot that match it's quality."
    -- Vendor screen instructions
    lt["VendorInstructions"] = "Items that should be deleted according to the quality settings will be kept if they vendor for more than the amount set below."
    -- Exemptions screen instructions
    lt["ExemptionsInstructions"] = "Entries should be seperated by new lines, contain no leading white spaces, and are case-sensitive."
    -- Help screen instructions
    lt["HelpInstructions"] = "Click the topic you need help with."
    -- Miscellaneous screen instructions
    lt["MiscellaneousInstructions"] = ""
    lt["MiscellaneousFooter"] = "* - In order for these settings to take effect you must press the SAVE button."
    
    -- Delete list title
    lt["Delete"] = "Delete"
    -- Do not delete list title
    lt["Do Not Delete"] = "Do Not Delete"
    
    
    lt["Gold"] = "gold"
    lt["Copper"] = "copper"
    lt["Silver"] = "silver"
    
    -- message in chat box when you receive loot.
    lt["You receive loot"] = "You receive loot"
    -- As returned by http://www.wowwiki.com/API_GetAuctionItemClasses
    lt["quest"] = "quest"
    
    -- misc Auto loot money checkbox
    lt["AutoLootCorpseMoney"] = "Automatically loot money from corpses."
    -- misc Disable inventory checkbox
    lt["DisableLootCleanse"] = "Disable inventory functions."
    -- misc Enable close with esc checkbox
    -- lt["EnableCloseWithEsc"] = "ESC key closes LootCleanse window."
    -- misc Save on close check box
    lt["EnableSaveOnClose"] = "Save settings when LootCleanse window closes.*"
    
    -- message displayed in help frame by default
    lt["Help-Default"] = "Click the topic you need help with."
    
    -- message displayed in help frame for quality info
    lt["Help-Quality1"] = "To delete items that match a certain quality, check the respective box."
    lt["Help-Quality2"] = "For example, if you have checked the Poor box and the Common box, you will" ..
                          " automatically delete all poor and common items you loot unless you" ..
                          " specified a poor or common item in the exemptions list."
    lt["Help-Quality3"] = ""
    
    -- message displayed in help frame for vendor info
    lt["Help-Vendor1"] = "To sell valuable items that would otherwise have been deleted set a minimum vendor value."
    lt["Help-Vendor2"] = "For example, if you put 2 1 3 in the boxes than any item that would have been deleted" ..
                         " but its worth more than 2 gold, 1 silver, and 3 copper will be kept."
    lt["Help-Vendor3"] = "You can also check the box below the minimum values edit boxes to automatically sell" ..
                         " the valuable items you have kept when you visit a merchant."
     
    -- message displayed in help frame for exemptions info
    lt["Help-Exemptions1"] = "Entries should be one per line, have no leading white spaces and are case-sensitive."
    lt["Help-Exemptions2"] = "For example, \"Linen CLoth\" is different from \"Linen Cloth\" and \" Wool Cloth\" is different" ..
                             " from \"Wool Cloth\"."
    lt["Help-Exemptions3"] = "If you are unsure of an item's spelling look it up before you continue to avoid unwanted" ..
                             " side effects such as accidentally deleting an item."
    
    -- message displayed in help frame for miscellaneous info
    lt["Help-Misc1"] = "Disable inventory functions turns off item deleting and selling."
    lt["Help-Misc2"] = ""
    lt["Help-Misc3"] = ""
    
end

lt["empty"] = ""

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
