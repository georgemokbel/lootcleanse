-- LC_Templates.lua
-- Contents.
-- 0. Templates
-- 1. Template Functions


lc.templates = { }

----------------------------------------------------------------------------
-- Templates ---------------------------------------------------------------
----------------------------------------------------------------------------

local locale = lc.localization

function ListButtonTemplate( name, parent, setTextValue, width, height )
    local width = width or 120
    local height = height or 24
    local temp = CreateFrame("Button", name, parent )
    temp:SetSize( width, height)
    temp:SetNormalFontObject(GameFontNormal)
    temp:RegisterForClicks("LeftButtonUp") 
    temp:SetText(locale[setTextValue])
    temp:SetBackdrop( 
      { 
        bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
        edgeFile = "", 
        true, 8, 0
      } 
    ) 
    temp:SetBackdropColor(0,0,0,.5)
    temp:SetBackdropBorderColor(167,167,167,.5)
    
    if width ~= 120 then
      temp:SetWidth(temp:GetTextWidth() + 8 )
      temp:SetNormalFontObject(GameFontGreen)
    end

    temp:SetHighlightFontObject(GameFontHighlight)
    temp:SetHighlightTexture("Interface\\DialogFrame\\UI-DialogBox-Gold-Background")
    temp:SetPushedTexture("Interface\\DialogFrame\\UI-DialogBox-Gold-Background")
   
    return temp
end
 
function EditBoxTemplate( name, parent )
  local temp = CreateFrame("EditBox", parent:GetName()..": "..name, parent, "InputBoxTemplate")
  temp:SetHeight(32)
  temp:SetWidth(24)
  temp:SetMaxLetters(2)
  temp:SetText("0")
  temp:SetNumeric(true)
  temp:SetAutoFocus(false)
  return temp
end

function ScrollFrameEditBox( name, parent )
  local temp = CreateFrame("EditBox", parent:GetName()..": "..name, parent )
  temp:SetFontObject(GameFontNormalSmall)
  temp:SetText("examples:\nLinen Cloth\nWool Cloth\nCopper Ore\nWhale Statue\n")
  temp:SetSize(140, 240)
  temp:SetMultiLine(true)
  temp:SetMaxLetters(99999)
  temp:SetTextColor(255,255,255,1)
  temp:SetTextInsets(4,4,0,4)
  temp:SetAutoFocus(false)
  
  temp.lastNumberOfLetters = temp:GetNumLetters()
  return temp
end



function CheckBoxButtonTemplate( name, parent )
 
  local temp = CreateFrame("CheckButton", parent:GetName()..": "..name, parent, "OptionsBaseCheckButtonTemplate")
  temp:SetSize(24, 24)
  temp:SetHitRectInsets(1,1,1,1)
  temp:SetNormalFontObject(GameFontNormal)
  temp:SetText(locale[name])
  local fs = temp:GetFontString()
  fs:SetPoint("LEFT", temp, "RIGHT", 2, 0 )
  
  local r,g,b = 255,255,255
  
  if name == "Poor" then
    r,g,b = GetItemQualityColor(0)
  elseif name == "Common" then
    r,g,b = GetItemQualityColor(1)
  elseif name == "Uncommon" then
    r,g,b = GetItemQualityColor(2)  
  elseif name == "Rare" then
    r,g,b = GetItemQualityColor(3)    
  elseif name == "Epic" then
    r,g,b = GetItemQualityColor(4)      
  elseif name == "Quest" then
    r,g,b = GetItemQualityColor(7)        
  end

  fs:SetTextColor(r,g,b,1)
  fs:SetWordWrap(true)
  fs:SetWidth(280)
  fs:SetJustifyH("LEFT")
  temp:SetFontString(fs)
  return temp
end

function HorizontalLineLeft( parent )
  local temp = parent:CreateTexture()
  temp:SetTexture(.5,.5,.5,.8)
  temp:SetGradient("HORIZONTAL", 0,0,0,.5,.5,.5)
  temp:SetSize( 64, 1 )
  return temp
end

function HorizontalLineRight( parent )
  local temp = parent:CreateTexture()
  temp:SetTexture(.5,.5,.5,.8)
  temp:SetGradient("HORIZONTAL", .5,.5,.5,0,0,0)
  temp:SetSize( 64, 1)
  return temp
end


----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- Template Functions ------------------------------------------------------
----------------------------------------------------------------------------



----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------