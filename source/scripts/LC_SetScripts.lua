-- LC_SetScripts.lua
-- Contents.
-- 0. Settings Scripts
-- 1. Logic Scripts

local settings = lc.frames.settings
local options = lc.frames.settings.options
local content = lc.frames.settings.content
local locale = lc.localization
local util= lc.utility
local logic = lc.frames.logic

----------------------------------------------------------------------------
-- Settings Scripts --------------------------------------------------------
----------------------------------------------------------------------------
settings:SetScript("OnEvent", OnEventSettingsFrame)
settings:SetScript("OnShow", OnShowMainFrame)
settings:SetScript("OnHide", OnHideMainFrame)


settings:SetScript("OnDragStart", settings.StartMoving)
settings:SetScript("OnDragStop", settings.StopMovingOrSizing)
settings:SetScript("OnShow", OnShowSettingsFrame)
settings:SetScript("OnHide", OnHideSettingsFrame)

settings.CloseButton:SetScript("OnClick", OnClickSettingsCloseButton)
settings.SaveButton:SetScript("OnClick", OnClickSettingsSaveButton)

options.QualityButton:SetScript("OnClick", OnClickQualityButton)
options.VendorButton:SetScript("OnClick", OnClickVendorButton)
options.ExemptionsButton:SetScript("OnClick", OnClickExemptionsButton)
options.MiscButton:SetScript("OnClick", OnClickMiscButton)
options.HelpButton:SetScript("OnClick", OnClickHelpButton)

content.DoNotDeleteEB:SetScript("OnCursorChanged", OnCursorChangedDoNotDeleteEB)
content.DoNotDeleteEB:SetScript("OnEscapePressed", OnEscapePressedDoNotDeleteEB)
content.DoNotDeleteEB:SetScript("OnEditFocusLost", OnEditFocusLostDoNotDeleteEB)

content.DeleteEB:SetScript("OnCursorChanged", OnCursorChangedDeleteEB)
content.DeleteEB:SetScript("OnEscapePressed", OnEscapePressedDeleteEB)
content.DeleteEB:SetScript("OnEditFocusLost", OnEditFocusLostDeleteEB)

content.GoldEB:SetScript("OnEditFocusLost", OnEditFocusLostVendorEB)
content.SilverEB:SetScript("OnEditFocusLost", OnEditFocusLostVendorEB)
content.CopperEB:SetScript("OnEditFocusLost", OnEditFocusLostVendorEB)

-- content.EnableCloseWithEscCB:SetScript("OnClick", OnClickEnableCloseWithEscCB)

content.HelpQualityButton:SetScript("OnClick", OnClickHelpQualityButton)
content.HelpExemptionsButton:SetScript("OnClick", OnClickHelpExemptionsButton)
content.HelpVendorButton:SetScript("OnClick", OnClickHelpVendorButton)
content.HelpMiscButton:SetScript("OnClick", OnClickHelpMiscButton)

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

----------------------------------------------------------------------------
-- Logic Scripts ------------------------------------------------------------
----------------------------------------------------------------------------

logic:SetScript("OnEvent", OnEventLogic)

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

