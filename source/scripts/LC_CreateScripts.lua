-- LC_CreateScripts
-- Contents
-- 0. Settings Scripts
-- 1. Help Page Scripts
-- 2. Logic Scripts

local settings = lc.frames.settings
local options = lc.frames.settings.options
local content = lc.frames.settings.content
local help = lc.frames.settings.content.helpmessage
local locale = lc.localization
local util= lc.utility

----------------------------------------------------------------------------
-- Settings Scripts --------------------------------------------------------
----------------------------------------------------------------------------

function OnEventSettingsFrame(self, event, ...)
  if event == "ADDON_LOADED" then
    if not lcsv then
      lcsv = {}
      util:InitializeSavedVariables()
    else
      util:LoadSavedVariables()
    end
    
    self:UnregisterEvent("ADDON_LOADED")
  elseif event == "PLAYER_LOGIN" then
    print("LootCleanse Version 0.01. " .. locale["PlayerLogin"])
    self:UnregisterEvent("PLAYER_LOGIN")
  end
end

function OnClickSettingsSaveButton()  
  lcsv.KEEP_POOR =not content.KeepPoorCB:GetChecked()
  lcsv.KEEP_COMMON = not content.KeepCommonCB:GetChecked()
  lcsv.KEEP_UNCOMMON = not content.KeepUncommonCB:GetChecked()
  lcsv.KEEP_RARE = not content.KeepRareCB:GetChecked()
  lcsv.KEEP_EPIC = not content.KeepEpicCB:GetChecked()
  lcsv.KEEP_QUEST = not content.KeepQuestCB:GetChecked()
  
  lcsv.VENDOR_MINIMUM_COPPER = tonumber(content.CopperEB:GetText())
  lcsv.VENDOR_MINIMUM_SILVER = tonumber(content.SilverEB:GetText())
  lcsv.VENDOR_MINIMUM_GOLD = tonumber(content.GoldEB:GetText())
  lcsv.AUTO_VENDOR = not not content.AutoVendorCB:GetChecked()
  lcsv.VENDOR_MINIMUM = (10000 * lcsv.VENDOR_MINIMUM_GOLD) + (100* lcsv.VENDOR_MINIMUM_SILVER) + lcsv.VENDOR_MINIMUM_COPPER
  
  util:ListSaver( content.DoNotDeleteEB:GetText().."\n", lcsv.DND_LIST, "\n" )
  util:ListSaver( content.DeleteEB:GetText().."\n", lcsv.DELETE_LIST, "\n" )
  
  lcsv.DISABLE_LOOTCLEANSE = not not content.DisableLootCleanseCB:GetChecked()
  -- lcsv.ENABLE_ESC_CLOSE = not not content.EnableCloseWithEscCB:GetChecked()
  lcsv.ENABLE_CLOSE_SAVE = not not content.EnableSaveOnCloseCB:GetChecked()
  lcsv.ENABLE_AUTOLOOT_MONEY = not not content.AutoLootCorpseMoneyCB:GetChecked()
  
  if lcsv.ENABLE_CLOSE_SAVE then
    settings.FOOTER_STRING:SetShown(false)
  else
    settings.FOOTER_STRING:SetShown(true)
  end
  
end


function OnShowSettingsFrame(self)
  SetContentToQuality()  
end

function OnHideSettingsFrame(self)
  if not not content.EnableSaveOnCloseCB:GetChecked() then
    OnClickSettingsSaveButton()
  end
end

function OnClickQualityButton(self, button)
  ChangeContent("quality")
end

function OnClickVendorButton(self, button)
  ChangeContent("vendor")
end

function OnClickExemptionsButton(self, button)
  ChangeContent("exemptions")
end

function OnClickMiscButton(self, button)
  ChangeContent("miscellaneous")
end

function OnClickHelpButton(self, button)
  ChangeContent("help")
end

function OnClickSettingsCloseButton(self, button)
  settings:Hide()
end


function ChangeContent( btn )
  if btn == "quality" then
    SetContentToQuality()
  elseif btn == "vendor" then
    SetContentToVendor()
  elseif btn == "exemptions" then
    SetContentToExemptions()
  elseif btn == "miscellaneous" then
    SetContentToMisc()
  elseif btn == "help" then
    SetContentToHelp()
  end
end

function SetContentToQuality()
  util:UnhideQualityLayout()
  util:HideVendorLayout()
  util:HideExemptionsLayout()
  util:HideMiscLayout()
  util:HideHelpLayout()
  
  options.QualityButton:SetNormalFontObject(GameFontWhite)
  options.VendorButton:SetNormalFontObject(GameFontNormal)
  options.ExemptionsButton:SetNormalFontObject(GameFontNormal)
  options.MiscButton:SetNormalFontObject(GameFontNormal)
  options.HelpButton:SetNormalFontObject(GameFontNormal)
  
  content.TITLE_STRING:SetText(locale["Quality"])
  content.INSTRUCTIONS_STRING:SetText(locale["QualityInstructions"])
end

function SetContentToVendor()
  util:UnhideVendorLayout()
  util:HideQualityLayout()
  util:HideExemptionsLayout()
  util:HideMiscLayout()
  util:HideHelpLayout()
  
  options.QualityButton:SetNormalFontObject(GameFontNormal)
  options.VendorButton:SetNormalFontObject(GameFontWhite)
  options.ExemptionsButton:SetNormalFontObject(GameFontNormal)
  options.MiscButton:SetNormalFontObject(GameFontNormal)
  options.HelpButton:SetNormalFontObject(GameFontNormal)
  
  content.TITLE_STRING:SetText(locale["Vendor"])
  content.INSTRUCTIONS_STRING:SetText(locale["VendorInstructions"])  
 
  
  
end

function SetContentToExemptions()
  util:UnhideExemptionsLayout()
  util:HideQualityLayout()
  util:HideVendorLayout()
  util:HideMiscLayout()
  util:HideHelpLayout()
  
  options.QualityButton:SetNormalFontObject(GameFontNormal)
  options.VendorButton:SetNormalFontObject(GameFontNormal)
  options.ExemptionsButton:SetNormalFontObject(GameFontWhite)
  options.MiscButton:SetNormalFontObject(GameFontNormal)
  options.HelpButton:SetNormalFontObject(GameFontNormal)
  
  content.TITLE_STRING:SetText(locale["Exemptions"])
  content.INSTRUCTIONS_STRING:SetText(locale["ExemptionsInstructions"])
  
end

function SetContentToMisc()
  util:UnhideMiscLayout()
  util:HideExemptionsLayout()
  util:HideQualityLayout()
  util:HideVendorLayout()
  util:HideHelpLayout()
  
  options.QualityButton:SetNormalFontObject(GameFontNormal)
  options.VendorButton:SetNormalFontObject(GameFontNormal)
  options.ExemptionsButton:SetNormalFontObject(GameFontNormal)
  options.MiscButton:SetNormalFontObject(GameFontWhite)
  options.HelpButton:SetNormalFontObject(GameFontNormal)
  
  content.TITLE_STRING:SetText(locale["Miscellaneous"])
  content.INSTRUCTIONS_STRING:SetText(locale["MiscellaneousInstructions"])
  
end

function SetContentToHelp()
  util:UnhideHelpLayout()
  util:HideMiscLayout()
  util:HideExemptionsLayout()
  util:HideQualityLayout()
  util:HideVendorLayout()
  
  
  options.QualityButton:SetNormalFontObject(GameFontNormal)
  options.VendorButton:SetNormalFontObject(GameFontNormal)
  options.ExemptionsButton:SetNormalFontObject(GameFontNormal)
  options.MiscButton:SetNormalFontObject(GameFontNormal)
  options.HelpButton:SetNormalFontObject(GameFontWhite)
  
  content.TITLE_STRING:SetText(locale["Help"])
  content.INSTRUCTIONS_STRING:SetText(locale["empty"])
end

-----------------------------------------------------------------------

--[[
function OnClickEnableCloseWithEscCB( self, button, down )
  local name = settings:GetName()
  if self:GetChecked() then
    tinsert(UISpecialFrames, name)
  else
    for k,v in pairs(UISpecialFrames) do
        if v == name then
          tremove(UISpecialFrames, k)
        end 
    end
  end 
end
]]

function OnCursorChangedDoNotDeleteEB(self, _, y, _, pixelHeight)
  local parent, y = self:GetParent(), -y
  local numLetters = self:GetNumLetters()
  local numLines = floor(y / pixelHeight) 
  if numLines > 22 and y > 200 then
    if numLetters ~= self.lastNumberOfLetters then
      parent:SetVerticalScroll(parent:GetVerticalScrollRange())
    end
  end
  self.lastNumberOfLetters = numLetters
end

function OnEscapePressedDoNotDeleteEB(self)
  self:ClearFocus()
end

function OnEditFocusLostDoNotDeleteEB(self)
  self:HighlightText(0,0)
  self:ClearFocus()
end


function OnCursorChangedDeleteEB(self, _, y, _, pixelHeight)
  local parent, y = self:GetParent(), -y
  local numLetters = self:GetNumLetters()
  local numLines = floor(y / pixelHeight) 
  if numLines > 22 and y > 200 then
    if numLetters ~= self.lastNumberOfLetters then
      parent:SetVerticalScroll(parent:GetVerticalScrollRange())
    end
  end
  self.lastNumberOfLetters = numLetters
end


function OnEscapePressedDeleteEB(self)
  self:ClearFocus()
end

function OnEditFocusLostDeleteEB(self)
  self:HighlightText(0,0)
  self:ClearFocus()
end

function OnEditFocusLostVendorEB(self)
  self:ClearFocus()
  if not tonumber(self:GetText()) then
    self:SetText(0)
  end
  self:HighlightText(0,0)
end



----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

----------------------------------------------------------------------------
-- Help Page Scripts -------------------------------------------------------
----------------------------------------------------------------------------

function OnClickHelpQualityButton(self)
  help.MESSAGE1:SetText(locale["Help-Quality1"])
  help.MESSAGE2:SetText(locale["Help-Quality2"])
  help.MESSAGE3:SetText(locale["Help-Quality3"])
end

function OnClickHelpExemptionsButton(self)
  help.MESSAGE1:SetText(locale["Help-Exemptions1"])
  help.MESSAGE2:SetText(locale["Help-Exemptions2"])
  help.MESSAGE3:SetText(locale["Help-Exemptions3"])
end

function OnClickHelpVendorButton(self)
  help.MESSAGE1:SetText(locale["Help-Vendor1"])
  help.MESSAGE2:SetText(locale["Help-Vendor2"])
  help.MESSAGE3:SetText(locale["Help-Vendor3"])
end

function OnClickHelpMiscButton(self)
  help.MESSAGE1:SetText(locale["Help-Misc1"])
  help.MESSAGE2:SetText(locale["Help-Misc2"])
  help.MESSAGE3:SetText(locale["Help-Misc3"])
end


----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- Logic Scripts -----------------------------------------------------------
----------------------------------------------------------------------------

function OnEventLogic(self, event, ...)
  if event == "LOOT_OPENED" and lcsv.ENABLE_AUTOLOOT_MONEY then
    OnLootOpened()
  elseif event == "CHAT_MSG_LOOT" and not lcsv.DISABLE_LOOTCLEANSE then
    OnChatMsgLoot(...)
  elseif event == "MERCHANT_SHOW" and not lcsv.DISABLE_LOOTCLEANSE then
    OnMerchantShow()
  end
end

-- Auto loot money from corpses.
function OnLootOpened()
  numItems = GetNumLootItems() -- get # of items present in loot window.
  for j = 1, numItems, 1 do
    lootType = GetLootSlotType( j )
    if lootType == 2 then
      LootSlot(j)
    end
  end
end

-- Loot deletion
function OnChatMsgLoot(...)
  local msg = ... 
  if string.find( msg, locale["You receive loot"] ) then
    -- Get item id
    local _,_, itemID = string.find( msg, ".*|Hitem:(%d+):.*")
    -- Get important information.
    local itemName, _, itemQuality, _, _, itemClass, _, stackSize, _, _, vendorPrice = GetItemInfo( itemID )  
    
    if util:ItemQualityMatchesDeleteQuality( itemQuality , itemClass ) then
      -- if it's not in the keep list or worth vendoring, add to delete list.
      -- combine the next two if else after debugging.
      if util:IsItemInList( "DO NOT DELETE", itemName ) then
        print("Keeping item " .. itemName)
      elseif util:IsItemWorthVendoring( vendorPrice ) then
        print(itemName .. " is worth keeping, saving to vendor.")
        util:AddItemToList( "VENDOR", itemName )
      else
        util:AddItemToList( "DELETE", itemName, stackSize )
      end
    elseif util:IsItemInList( "DELETE", itemName ) then      
      util:AddItemToList( "DELETE", itemName, stackSize )
    end
    
  end
  
  if lcsv.ITEMS_TO_BE_DELETED_SIZE > 0 then
    util:HandleJunk("DELETE")
  end
end

function OnMerchantShow()
  if lcsv.ITEMS_TO_BE_VENDORED_SIZE > 0 then
    util:HandleJunk("VENDOR")
  end
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
