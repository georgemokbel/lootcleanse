-- LC_UtilityScripts.lua
-- Contents.
-- 0. Content Layout Utilities
-- 1. String Utilities
-- 2. Logic Utilities
-- 3. Addon Utilities
-- 4. Debug Utilities

local settings = lc.frames.settings
local options = lc.frames.settings.options
local content = lc.frames.settings.content
local help = lc.frames.settings.content.helpmessage
local logic = lc.frames.logic
local utility = lc.utility
local locale = lc.localization

----------------------------------------------------------------------------
-- Content Layout Utilities ------------------------------------------------
----------------------------------------------------------------------------

function utility:HideQualityLayout()
  content.KeepPoorCB:SetShown(false)
  content.KeepCommonCB:SetShown(false)
  content.KeepUncommonCB:SetShown(false)
  content.KeepRareCB:SetShown(false)
  content.KeepEpicCB:SetShown(false)
  content.KeepQuestCB:SetShown(false)
end

function utility:UnhideQualityLayout()
  content.KeepPoorCB:SetShown(true)
  content.KeepCommonCB:SetShown(true)
  content.KeepUncommonCB:SetShown(true)
  content.KeepRareCB:SetShown(true)
  content.KeepEpicCB:SetShown(true)
  content.KeepQuestCB:SetShown(true)
end


function utility:HideVendorLayout()
  content.GoldEB:SetShown(false)
  content.GOLD_ICON:SetShown(false)
  content.SilverEB:SetShown(false)
  content.SILVER_ICON:SetShown(false)
  content.CopperEB:SetShown(false)
  content.COPPER_ICON:SetShown(false)
  content.AutoVendorCB:SetShown(false)
end

function utility:UnhideVendorLayout()
  content.GoldEB:SetShown(true)
  content.GOLD_ICON:SetShown(true)
  content.SilverEB:SetShown(true)
  content.SILVER_ICON:SetShown(true)
  content.CopperEB:SetShown(true)
  content.COPPER_ICON:SetShown(true)
  content.AutoVendorCB:SetShown(true)
end

function utility:HideExemptionsLayout()
  content.DND_STRING:SetShown(false)
  content.DELETE_STRING:SetShown(false)
  content.DoNotDeleteSF:SetShown(false) 
  content.DeleteSF:SetShown(false)
end

function utility:UnhideExemptionsLayout()
  content.DND_STRING:SetShown(true)
  content.DELETE_STRING:SetShown(true)
  content.DoNotDeleteSF:SetShown(true)
  content.DeleteSF:SetShown(true)
  content.DoNotDeleteEB:SetShown(true)
  content.DeleteEB:SetShown(true)
  utility:PopulateEditBox("Do Not Delete")
  utility:PopulateEditBox("Delete")
end

function utility:HideMiscLayout()
  content.AutoLootCorpseMoneyCB:SetShown(false)
  content.DisableLootCleanseCB:SetShown(false)
  -- content.EnableCloseWithEscCB:SetShown(false)
  content.EnableSaveOnCloseCB:SetShown(false)
  content.MISC_FOOTER:SetShown(false)
end

function utility:UnhideMiscLayout()
  content.AutoLootCorpseMoneyCB:SetShown(true)
  content.DisableLootCleanseCB:SetShown(true)
  -- content.EnableCloseWithEscCB:SetShown(true)
  content.EnableSaveOnCloseCB:SetShown(true)
  content.MISC_FOOTER:SetShown(true)
end

function utility:HideHelpLayout()
  help:SetShown(false)
  help.MESSAGE1:SetShown(false)
  help.MESSAGE2:SetShown(false)
  help.MESSAGE3:SetShown(false)
  content.HelpQualityButton:SetShown(false)
  content.HelpVendorButton:SetShown(false)
  content.HelpExemptionsButton:SetShown(false)
  content.HelpMiscButton:SetShown(false)
  content.HELP_HL1_LEFT:SetShown(false)
  content.HELP_HL1_RIGHT:SetShown(false)
end

function utility:UnhideHelpLayout()
  help:SetShown(true)
  help.MESSAGE1:SetShown(true)
  help.MESSAGE2:SetShown(true)
  help.MESSAGE3:SetShown(true)
  content.HelpQualityButton:SetShown(true)
  content.HelpVendorButton:SetShown(true)
  content.HelpExemptionsButton:SetShown(true)
  content.HelpMiscButton:SetShown(true)
  content.HELP_HL1_LEFT:SetShown(true)
  content.HELP_HL1_RIGHT:SetShown(true)
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- String Utilities --------------------------------------------------------
----------------------------------------------------------------------------

function utility:PopulateEditBox( name )
  local editBoxContent = ""
  
  if name == "Do Not Delete" then
    for k in pairs(lcsv.DND_LIST) do
      editBoxContent = editBoxContent .. k .. "\n"
      content.DoNotDeleteEB:SetText(editBoxContent)
    end
  elseif name == "Delete" then
    for k in pairs(lcsv.DELETE_LIST) do
      editBoxContent = editBoxContent .. k .. "\n"
      content.DeleteEB:SetText(editBoxContent)
    end
  end 
end

-- @param - items, string list of items
-- @param - t, table in which to insert split strings.
-- @param - delim, delimitting character.
function utility:ListSaver( items, t, delim )
    -- destroy the table.
    for k in pairs(t) do t[k] = nil end
    
    local current -- current string value
    local count = string.len(items) -- length of string to be parsed
    local pos = 0 -- where we currently are on the string being parsed.
    local lastPos = 1 -- the last spot we encountered a delimitter.
    
    while true do
        -- find the first delimiter, must start @ 1 since lua starts counting at 1, hence pos+1
        pos = items.find( items, delim, pos+1 )
        
        -- we encountered a delimiter, extra string info up to this point.
        if pos ~= nil then       
            -- ignore trailing delims if we find them
            local trailingDelims = items.find( items, "[^"..delim.."]", pos-1 )
            
            -- if no more trailing delims, we may have hit the end of the string. 
            if trailingDelims == nil then
              -- check to see if characters remain in the string ( incase one item only present in string )
              if pos < count then
                -- get the remaining characters
                --cur = string.sub(items, lastPos, pos-1)
                cur = string.sub(items, lastPos, count)
                -- set index to true.
                t[cur] = true
              break
              end
            else
              -- if we find an alphanumeric character 
              if string.match( string.sub( items, lastPos, pos-1), "%w-[^"..delim.."]") then
                  cur = string.sub(items, lastPos, pos-1)
                  t[cur] = true
              end
              -- update our lastencountered delimiter to the next character after the last saved string's last character.
              lastPos = pos+1
            end
        else         
          break
        end
    end     
end

--@param - string, item hyperLink 
--@return -number, itemID of item.
function utility:GetItemIdFromItemLink( itemLink )
	return tonumber(string.match(itemLink, ":(%d+)"))
end

--@param - string, item hyperLink
--@return - string, the item name as a string stripped of hyperlink markup
function utility:GetItemNameFromItemLink( itemLink )
  return string.match(itemLink, "%[(.*)%]")
end


--@param - string, the name of the list
--@param - string, the name of the item.
--@return - boolean, true if item is in the list
function utility:IsItemInList( list, itemName )
  if list == "DO NOT DELETE" then
    if lcsv.DND_LIST[itemName] then
      return true
    end
  elseif list == "DELETE" then
    if lcsv.DELETE_LIST[itemName] then
      return true
    end
  end
end


--@param - string, the name of the list
--@param - string, name of item to be vendored.
--@param - (optional) number, the maximum stack size.
function utility:AddItemToList( list, itemName, stackSize )
  if list == "VENDOR" then
    if lcsv.ITEMS_TO_BE_VENDORED[itemName] == nil then
        lcsv.ITEMS_TO_BE_VENDORED[itemName] = true
        lcsv.ITEMS_TO_BE_VENDORED_SIZE = lcsv.ITEMS_TO_BE_VENDORED_SIZE + 1
    end
  elseif list == "DELETE" then
    if stackSize > 1 or lcsv.ITEMS_TO_BE_DLETED[itemName] == nil then
      lcsv.ITEMS_TO_BE_DELETED[itemName] = true
      lcsv.ITEMS_TO_BE_DELETED_SIZE = lcsv.ITEMS_TO_BE_DELETED_SIZE + 1
    end
  end
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------



----------------------------------------------------------------------------
-- Logic Utilities ---------------------------------------------------------
----------------------------------------------------------------------------

--@param - number, the quality of the item, as returned by GetItemInfo
--@return - boolean, true if the item matches the qualities we are deleting.
function utility:ItemQualityMatchesDeleteQuality( itemQuality, itemClass )
  
  local POOR_QUALITY = 0
  local COMMON_QUALITY = 1
  local UNCOMMON_QUALITY = 2
  local RARE_QUALITY = 3
  local EPIC_QUALITY = 4
  if itemClass == locale["quest"] and not lcsv.KEEP_QUEST then
    return true
  end
  
  if itemQuality == POOR_QUALITY and not lcsv.KEEP_POOR then
    return true
  elseif itemQuality == COMMON_QUALITY and not lcsv.KEEP_COMMON then
    return true
  elseif itemQuality == UNCOMMON_QUALITY and not lcsv.KEEP_UNCOMMON then
    return true
  elseif itemQuality == RARE_QUALITY and not lcsv.KEEP_RARE then
    return true
  elseif itemQuality == EPIC_QUALITY and not lcsv.KEEP_EPIC then
    return true
  else
    return false
  end
end

--@param - number, the vendor price of the item
--@return - boolean, true if item is worth keeping to vendor.
function utility:IsItemWorthVendoring( vendorPrice )
  if vendorPrice >= lcsv.VENDOR_MINIMUM then
    return true
  end
  return false
end

--@param - string, the type of junk to be deleted, can be VENDOR, DELETE
function utility:HandleJunk( junkType )
  for i = 0, 4, 1 do
    local containerSlots = GetContainerNumSlots(i)
    for j = 1, containerSlots, 1 do
      local value = GetContainerItemLink(i, j)
      if value then
        local itemName = utility:GetItemNameFromItemLink(value)
        if junkType == "DELETE" then
          if lcsv.ITEMS_TO_BE_DELETED[itemName] then
           PickupContainerItem(i, j)
           if CursorHasItem() then
              DeleteCursorItem()
              ClearCursor()
              lcsv.ITEMS_TO_BE_DELETED[itemName] = nil
              lcsv.ITEMS_TO_BE_DELETED_SIZE = lcsv.ITEMS_TO_BE_DELETED_SIZE - 1
           end
          end
        elseif junkType == "VENDOR" then
          if lcsv.ITEMS_TO_BE_VENDORED[itemName] then
              PickupContainerItem(i,j)
              if CursorHasItem() then
                ShowContainerSellCursor(i, j)
                UseContainerItem(i, j)
                ClearCursor()
                lcsv.ITEMS_TO_BE_VENDORED[itemName] = nil
                lcsv.ITEMS_TO_BE_VENDORED_SIZE = lcsv.ITEMS_TO_BE_VENDORED_SIZE - 1
              end
          end 
        end         
      end
    end
  end
end


function utility:DeleteUnwantedJunk()
  for i = 0, 4, 1 do
    local containerSlots = GetContainerNumSlots(i)
    for j = 1, containerSlots, 1 do
      -- Iterate the bag searching for items that match the ITEMS_TO_BE_DELETED value
      local value = GetContainerItemLink(i, j)
      if value then
        local itemName = utility:GetItemNameFromItemLink(value)
        if lcsv.ITEMS_TO_BE_DELETED[itemName] then
           PickupContainerItem(i, j)
           if CursorHasItem() then
              DeleteCursorItem()
              ClearCursor()
              lcsv.ITEMS_TO_BE_DELETED[itemName] = nil
              lcsv.ITEMS_TO_BE_DELETED_SIZE = lcsv.ITEMS_TO_BE_DELETED_SIZE - 1
           end
        end
      end
    end
  end
end

function utility:SellUnwantedJunk()
  for i = 0, 4, 1 do
      local containerSlots = GetContainerNumSlots(i)
      for j = 1, containerSlots, 1 do
          local value = GetContainerItemLink(i,j)
          if value then
              local itemName = GetItemNameFromItemLink(value)
              
              if lcsv.ITEMS_TO_BE_VENDORED[itemName] then
                  PickupContainerItem(i,j)
                  if CursorHasItem() then
                      ShowContainerSellCursor(i, j)
                      UseContainerItem(i, j)
                      ClearCursor()
                      lcsv.ITEMS_TO_BE_VENDORED[itemName] = nil
                      lcsv.ITEMS_TO_BE_VENDORED_SIZE = lcsv.ITEMS_TO_BE_VENDORED_SIZE - 1
                  end
              end             
          end          
      end     
  end
end


----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- Addon Utilities ---------------------------------------------------------
----------------------------------------------------------------------------

function utility:InitializeSavedVariables()

  lcsv.KEEP_POOR = true
  lcsv.KEEP_COMMON = true
  lcsv.KEEP_UNCOMMON = true
  lcsv.KEEP_RARE = true
  lcsv.KEEP_EPIC = true
  lcsv.KEEP_QUEST = true
  
  lcsv.VENDOR_MINIMUM_COPPER = 0
  lcsv.VENDOR_MINIMUM_SILVER = 0
  lcsv.VENDOR_MINIMUM_GOLD = 0
  lcsv.VENDOR_MINIMUM = 0
  lcsv.AUTO_VENDOR = true
  
  lcsv.DND_LIST = {}
  lcsv.DELETE_LIST = {}
  
  lcsv.ITEMS_TO_BE_VENDORED = {}
  lcsv.ITEMS_TO_BE_VENDORED_SIZE = 0
  lcsv.ITEMS_TO_BE_DELETED = {}
  lcsv.ITEMS_TO_BE_DELETED_SIZE = 0
  
  lcsv.DISABLE_LOOTCLEANSE = false
  lcsv.ENABLE_ESC_CLOSE = false
  lcsv.ENABLE_CLOSE_SAVE = true
  lcsv.ENABLE_AUTOLOOT_MONEY = true
  
end

function utility:LoadSavedVariables()
  
  if lcsv.KEEP_POOR == true then
    content.KeepPoorCB:SetChecked(false)
  else
    content.KeepPoorCB:SetChecked(true)
  end
  
  if lcsv.KEEP_COMMON == true then
    content.KeepCommonCB:SetChecked(false)
  else
    content.KeepCommonCB:SetChecked(true)
  end
  
  if lcsv.KEEP_UNCOMMON == true then
    content.KeepUncommonCB:SetChecked(false)
  else
    content.KeepUncommonCB:SetChecked(true)
  end
  
  if lcsv.KEEP_RARE == true then
    content.KeepRareCB:SetChecked(false)
  else
    content.KeepRareCB:SetChecked(true)
  end
  
  if lcsv.KEEP_EPIC == true then
    content.KeepEpicCB:SetChecked(false)
  else
    content.KeepEpicCB:SetChecked(true)
  end
  
  if lcsv.KEEP_QUEST == true then
    content.KeepQuestCB:SetChecked(false)
  else
    content.KeepQuestCB:SetChecked(true)
  end
  
  content.CopperEB:SetText(tostring(lcsv.VENDOR_MINIMUM_COPPER))
  content.SilverEB:SetText(tostring(lcsv.VENDOR_MINIMUM_SILVER))
  content.GoldEB:SetText(tostring(lcsv.VENDOR_MINIMUM_GOLD))
  
  if lcsv.AUTO_VENDOR == true then
    content.AutoVendorCB:SetChecked(true)
  else
    content.AutoVendorCB:SetChecked(false)
  end
  
  if lcsv.DISABLE_LOOTCLEANSE then
    content.DisableLootCleanseCB:SetChecked(true)
  else
    content.DisableLootCleanseCB:SetChecked(false)
  end
  
  if lcsv.ENABLE_CLOSE_SAVE then
    content.EnableSaveOnCloseCB:SetChecked(true)
    settings.FOOTER_STRING:SetShown(false)
  else
    content.EnableSaveOnCloseCB:SetChecked(false)
    settings.FOOTER_STRING:SetShown(true)
  end
  
  if lcsv.ENABLE_AUTOLOOT_MONEY then
    content.AutoLootCorpseMoneyCB:SetChecked(true)
  else
    content.AutoLootCorpseMoneyCB:SetChecked(false)
  end
  
end


function utility:DebugPrint()
   print("lcsv.ENABLE_ESC_CLOSE = " .. tostring(lcsv.ENABLE_ESC_CLOSE))
end



----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------