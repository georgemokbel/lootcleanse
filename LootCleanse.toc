## Interface: 50400
## Title: LootCleanse
## Notes: Automatically frees your bag space from loot you don't want.
## DefaultState: enabled
## Author: Siqbyte
## Version: 0.10

## SavedVariablesPerCharacter: lcsv

LootCleanse.lua

source\locale\LC_Localization.lua

source\templates\LC_Templates.lua

source\resources\LC_DeclareResources.lua
source\resources\LC_InitializeResources.lua
source\resources\LC_SetLayout.lua

source\interaction\LC_SetInteraction.lua

source\registration\LC_RegisterWidgets.lua

source\scripts\LC_UtilityScripts.lua
source\scripts\LC_CreateScripts.lua
source\scripts\LC_SetScripts.lua

source\slashcmds\LC_SlashCommands.lua


