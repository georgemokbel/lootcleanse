-- LootCleanse.lua
-- Contents
-- 0. Foreword
-- 1. Credits
-- 2. Namespace Initialization
-- 3. Debugging

----------------------------------------------------------------------------
---- Foreword --------------------------------------------------------------
----------------------------------------------------------------------------
  --[[

  I would like to thank you for your interest in LootCleanse and it's source 
  code. My name is Siqbyte and I am the developer of LootCleanse. This addon 
  is free to use and free to modify to your liking.
 
  Official versions of the addon are contained on my website. If you downloaded 
  this addon from somewhere else you should compare the code with the code 
  from my website, located at www.siqbyte.com
 
  Each iteration of LootCleanse will contain a changelog to keep you up to 
  date on what has changed.

  Suggestions / Comments can be sent to comments@siqbyte.com

  ]]
----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

----------------------------------------------------------------------------
---- Credits ---------------------------------------------------------------
----------------------------------------------------------------------------
  --[[
  
  Credits:
  
  Author of HixLua - For showing me how to get started on creating an addon
    strictly in LUA.
  Author of LootFilter - The inspiration for LootControl and certain ideas on
    how to handle specific things.
  Mainters and Contributers of WoWWiki - You people are awesome! Thanks for
    keeping an up to date reference!
  Authors of World of Warcraft Programming, 2nd ed. - Thank you for peaking my
    interest in the LUA programming language!
  
  To anyone else I may have forgotten, thank you. If you believe you earned a 
  spot in my credits please e-mail me and I will include you in the next iteration.
  
  To my knowledge I have copied zero lines of code, only used others works for
  references / ideas.
  
  ]]
----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

----------------------------------------------------------------------------
-- Namespace Initialization ------------------------------------------------
----------------------------------------------------------------------------

-- Ignore first parameter, take second ( namespace )
local _, ns = ... 
LootCleanse = LootCleanse or { }
LootCleanse.ns = ns
-- Store global handle to the namespace as lc
_G["lc"] = LootCleanse.ns

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- Debugging ---------------------------------------------------------------
----------------------------------------------------------------------------



----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
